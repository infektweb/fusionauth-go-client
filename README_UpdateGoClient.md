## Steps to uddate/upgrade your own fusion-auth go client

If you like to upgrade the fusion-auth-go-client to the lastest version follow the step below:

1. Checkout master branch and create a new branch

   - you may use cli or a git client like SmartGit: feature/updateVersionX.ab.c

2. Add remote git repo (from which you would like to receive the latest version)

   - git remote add upstreamGoClient https://github.com/FusionAuth/go-client.git (you have to check the repo url!)
   - git fetch upstreamGoClient

3. Check if your branch is checked out

   - git show

4. Merge new branch with remote git repo. (Do not rebase otherwise it's all for nothing: your old version is above the latest one)

   - git merge upstreamGoClient/master

5. Resolve conflicts (mainly take theirs and stage it).

   - Be aware not to delete by accident your own old changes.

6. Add additionally your own changes

   - add changes and comment it (on changeLog.txt as well)

7. Commit and push the merged verison (you may use SmartGit or cli)

   - git push -f origin <branch name>

8. Go to GitLab

   - create a merge request and finish it

9. Create a tag

   - create a version tag (The tag is necessary so that you can easily specify them in the go.mod)

```

```
